# Managing environments and kernels on this Jupyter-workspace

__On this workspace a custom kernel is installed, next to the default-kernel.__

__The installation of the custom kernel has been done with (mini-)conda and ipykernel.__

__This README will help you to modify a given kernel.__

## miniconda

Conda is the package-manager of the Anaconda ecosystem (anaconda.com).

Miniconda is the lean standalone-installation of conda.

We use miniconda to create virtual python environments.

Virtual environemts help to maintain reproducible setups for all your code.

## ipykernel

Ipykernel is a python-tool that can translate virtual environments into IPython-Kernels.

(IPython is the base of Jupyter notebooks)

That way, also the setup of your Jupyter kernels is maintainable and reproducible.

## Modifying the installed custom-kernel

If you want to modify any kernel in this workspace you can clone the respective conda-environemt to your home-directory.

You can make changes to that new environment-instance.

Then you can create a new kernel from it.

### Clone the custom-environment to your home-directory

#### Initialize conda

To make the already installed conda-tool available for yourself, you have to initialize your terminal shell.

Start a "Terminal" tab in the Jupyter Lab launcher and type:
```
/etc/miniconda/bin/conda init
```
Close the terminal tab and start a new one.
You will see that the terminal prompt has changed to something like
```
(base) metheuser@mywsp:
```
This is conda telling you that you are currently in the "base" environment.

You should not use for your code-development, though.

You can create and activate another environment for your project.

This is outside the scope of this README but you can read up on using conda in the [conda-docs](https://docs.conda.io/en/latest/).

#### Clone the existing environment

To find the exact name of the environment you want to clone, execute
```
conda env list
```
Clone this environment for yourself with
```
conda create --yes --name <your-new-environment-name> --clone <env-name-from-the-list>
```
#### Activate your new environment
```
conda activate <your-new-environment-name>
```
Now the terminal prompt shows
```
(<your-new-environment-name>) metheuser@mywsp:
```
#### Modify the environment
Please refer to the [conda-documentation](https://docs.conda.io/en/latest/) to learn how you can modify the environment.

##### View environemnt:
If you want to see what the currently activated environment contains, you can execute
```
conda list
```
Or to get a yaml file with wich you can re-create the environment elsewhere:
```
conda export > my-exported-environment-file.yml
```
(Put the file on persistent storage so it does not get lost together with your workspace!)

### Create a new kernel
If you are done with your modifications to the environment, you can create a kernel from it.

(Make sure that the modified conda-environment is still the active one.)
```
python -m ipykernel install --user --name <my-new-kernel-name> --display-name "<My friendly kernel name>"
```
This kernel will be selectable in the Jupyter Lab launcher after a few seconds.
